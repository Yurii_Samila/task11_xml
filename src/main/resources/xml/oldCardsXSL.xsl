<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
        <div style="background-color: green; color: black;">
          <h2>CARDS</h2>
        </div>
        <table border="3">
          <tr bgcolor="#2E9AFE">
            <th>Theme</th>
            <th>Type</th>
            <th>Sent</th>
            <th>Country</th>
            <th>Year</th>
            <th>Author</th>
            <th>Valuables</th>
          </tr>

          <xsl:for-each select="oldCards/card">
            <tr>
              <td><xsl:value-of select="theme"/></td>
              <td><xsl:value-of select="types/type"/></td>
              <td><xsl:value-of select="types/sent"/></td>
              <td><xsl:value-of select="country"/></td>
              <td><xsl:value-of select="year"/></td>
              <td><xsl:value-of select="author"/></td>
              <td><xsl:for-each select="valuables/valuable">
                <xsl:value-of select="."/>
                <xsl:text>, </xsl:text>
              </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>

