package com.task15_XML.model;

import java.util.Objects;

public class Valuables {

  private String valuable;

  public String getValuable() {
    return valuable;
  }

  public void setValuable(String valuable) {
    this.valuable = valuable;
  }

  public Valuables(String valuable) {
    this.valuable = valuable;
  }

  public Valuables() {
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Valuables valuable1 = (Valuables) o;
    return Objects.equals(valuable, valuable1.valuable);
  }

  @Override
  public int hashCode() {
    return Objects.hash(valuable);
  }

  @Override
  public String toString() {
    return "Valuables{" +
        "valuable='" + valuable + '\'' +
        '}';
  }
}
