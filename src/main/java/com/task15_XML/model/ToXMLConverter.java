package com.task15_XML.model;

import com.task15_XML.parser.stax.RunStax;
import java.io.File;
import java.util.Collections;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class ToXMLConverter {

  public static void convert() {
    RunStax stax = new RunStax();

    CardComparator comparator = new CardComparator();
    List<Card> parsedCards = stax.parsing();
    Collections.sort(parsedCards, comparator);
    oldCards cards = new oldCards();
    cards.setCards(parsedCards);
    convert(cards);
  }

  private static void convert(oldCards cards){
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(oldCards.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      File file = new File("src\\main\\resources\\xml\\oldCardsSorted.xml");
      jaxbMarshaller.marshal(cards, file);
    }catch (JAXBException e)
    {
      e.printStackTrace();
    }
  }

}
