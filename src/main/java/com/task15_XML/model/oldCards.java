package com.task15_XML.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "oldCards")
@XmlAccessorType(XmlAccessType.FIELD)
public class oldCards {
  @XmlElement(name = "card")
  private List<Card> cards = new ArrayList<>();

  public List<Card> getCards() {
    return cards;
  }

  public void setCards(List<Card> cards) {
    this.cards = cards;
  }

  @Override
  public String toString() {
    return "oldCards{" +
        "cards=" + cards +
        '}';
  }
}
