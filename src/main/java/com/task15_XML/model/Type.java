package com.task15_XML.model;

public enum Type {
  GREETINGS, ADVERTISING, ORDINARY
}
