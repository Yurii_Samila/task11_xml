package com.task15_XML.model;

import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "card")
@XmlAccessorType(XmlAccessType.FIELD)
public class Card {

  private String theme;
  private Types types;
  private String country;
  private int year;
  private String author;
  private List<Valuables> valuables;

  public Card(String theme, Types type, String country, int year, String author,
      List<Valuables> valuables) {
    this.theme = theme;
    this.types = type;
    this.country = country;
    this.year = year;
    this.author = author;
    this.valuables = valuables;
  }

  public Card() {
  }

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public Types getType() {
    return types;
  }

  public void setType(Types type) {
    this.types = type;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public List<Valuables> getValuables() {
    return valuables;
  }

  public void setValuables(List<Valuables> valuables) {
    this.valuables = valuables;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Card card = (Card) o;
    return year == card.year &&
        Objects.equals(theme, card.theme) &&
        Objects.equals(types, card.types) &&
        Objects.equals(country, card.country) &&
        Objects.equals(author, card.author) &&
        Objects.equals(valuables, card.valuables);
  }

  @Override
  public int hashCode() {
    return Objects.hash(theme, types, country, year, author, valuables);
  }

  @Override
  public String toString() {
    return "Card{" +
        "theme='" + theme + '\'' +
        ", types=" + types +
        ", country='" + country + '\'' +
        ", year=" + year +
        ", author='" + author + '\'' +
        ", valuables=" + valuables +
        '}';
  }
}
