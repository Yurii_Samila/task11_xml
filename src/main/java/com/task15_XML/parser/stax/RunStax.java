package com.task15_XML.parser.stax;

import com.task15_XML.model.Card;
import java.io.File;
import java.util.List;

public class RunStax {

  public List<Card> parsing() {
    File xmlFile = new File("src\\main\\resources\\xml\\oldCards.xml");
    StaxParser staxParser = new StaxParser();
    return staxParser.parse(xmlFile);
  }
  public static void run() {
    File xmlFile = new File("src\\main\\resources\\xml\\oldCards.xml");
    StaxParser staxParser = new StaxParser();
    System.out.println(staxParser.parse(xmlFile));
  }
}
