package com.task15_XML.parser.stax;

import com.task15_XML.model.Card;
import com.task15_XML.model.Type;
import com.task15_XML.model.Types;
import com.task15_XML.model.Valuables;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxParser {

  static final String CARD_TAG = "card";
  static final String THEME_TAG = "theme";
  static final String TYPES_TAG = "types";
  static final String TYPE_TAG = "type";
  static final String SENT_TAG = "sent";
  static final String COUNTRY_TAG = "country";
  static final String YEAR_TAG = "year";
  static final String AUTHOR_TAG = "author";
  static final String VALUABLES_TAG = "valuables";
  static final String VALUABLE_TAG = "valuable";

  private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

  private List<Card> cards = new ArrayList<>();
  private Card card;
  private Types type;
  private List<Valuables> valuables = new ArrayList<>();

  public List<Card> parse(File xml){
    try {
      XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()){
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()){
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name){
            case CARD_TAG:
             card = new Card();
             break;
            case THEME_TAG:
            xmlEvent = xmlEventReader.nextEvent();
            card.setTheme(xmlEvent.asCharacters().getData());
            break;
            case TYPES_TAG:
              type = new Types();
              break;
            case TYPE_TAG:
              xmlEvent = xmlEventReader.nextEvent();
              String cardTypeString = xmlEvent.asCharacters().getData();
              Type cardType = Type.valueOf(cardTypeString.toUpperCase());
              type.setType(cardType);
              break;
            case SENT_TAG:
              xmlEvent = xmlEventReader.nextEvent();
              type.setSent(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            case COUNTRY_TAG:
              xmlEvent = xmlEventReader.nextEvent();
              card.setCountry(xmlEvent.asCharacters().getData());
              break;
            case YEAR_TAG:
              xmlEvent = xmlEventReader.nextEvent();
              card.setYear(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case AUTHOR_TAG:
              xmlEvent = xmlEventReader.nextEvent();
              card.setAuthor(xmlEvent.asCharacters().getData());
              break;
            case VALUABLES_TAG:
              valuables = new ArrayList<>();
              break;
            case VALUABLE_TAG:
              xmlEvent = xmlEventReader.nextEvent();
              Valuables valuablesNew = new Valuables();
              valuablesNew.setValuable(xmlEvent.asCharacters().getData());
              valuables.add(valuablesNew);
              break;
          }
        }
        if (xmlEvent.isEndElement()){
          EndElement endElement = xmlEvent.asEndElement();
          String name = endElement.getName().getLocalPart();
            switch (name){
              case CARD_TAG:
              cards.add(card);
              card = null;
              break;
              case TYPES_TAG:
              card.setType(type);
              type = null;
              break;
              case VALUABLES_TAG:
                card.setValuables(valuables);
                valuables = null;
                break;
            }
        }
      }
    }catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    return cards;
  }

}
