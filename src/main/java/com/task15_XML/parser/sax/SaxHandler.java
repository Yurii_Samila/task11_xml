package com.task15_XML.parser.sax;

import com.task15_XML.model.Card;
import com.task15_XML.model.Type;
import com.task15_XML.model.Types;
import com.task15_XML.model.Valuables;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHandler extends DefaultHandler {

  static final String CARD_TAG = "card";
  static final String THEME_TAG = "theme";
  static final String TYPES_TAG = "types";
  static final String TYPE_TAG = "type";
  static final String SENT_TAG = "sent";
  static final String COUNTRY_TAG = "country";
  static final String YEAR_TAG = "year";
  static final String AUTHOR_TAG = "author";
  static final String VALUABLES_TAG = "valuables";
  static final String VALUABLE_TAG = "valuable";

  private List<Card> cards = new ArrayList<>();
  private Card card;
  private Types type;
  private List<Valuables> valuables = new ArrayList<>();
  private String currentElement;

  public List<Card> getCards(){
    return this.cards;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    currentElement = qName;
    switch (currentElement){
      case CARD_TAG:
        card = new Card();
        break;
      case TYPES_TAG:
        type = new Types();
        break;
      case VALUABLES_TAG:
        valuables = new ArrayList<>();
        break;
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    switch (qName){
      case CARD_TAG:
        cards.add(card);
        break;
      case TYPES_TAG:
        card.setType(type);
        type = null;
        break;
      case VALUABLES_TAG:
        card.setValuables(valuables);
        valuables = null;
        break;
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (currentElement.equals(THEME_TAG)){
      card.setTheme(new String(ch, start, length));
    }
    if (currentElement.equals(COUNTRY_TAG)){
      card.setCountry(new String(ch, start, length));
    }
    if (currentElement.equals(YEAR_TAG)){
      card.setYear(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(AUTHOR_TAG)){
      card.setAuthor(new String(ch, start, length));
    }
    if (currentElement.equals(TYPE_TAG)){
      String CardType = new String(ch, start, length);
      com.task15_XML.model.Type valueOfCardType = com.task15_XML.model.Type
          .valueOf(CardType.toUpperCase());
      type.setType(valueOfCardType);
    }
    if (currentElement.equals(SENT_TAG)){
      type.setSent(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if (currentElement.equals(VALUABLE_TAG)){
      Valuables valuablesNew = new Valuables();
      valuablesNew.setValuable(new String(ch, start, length));
      valuables.add(valuablesNew);
    }
  }
}
