package com.task15_XML.parser.sax;

import com.task15_XML.model.Card;
import java.io.File;
import java.util.List;
import javax.xml.parsers.SAXParser;

public class RunSax {

  public static void run() {
    File xmlFile = new File("src\\main\\resources\\xml\\oldCards.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\oldCardsXSD.xsd");
    List<Card> cards = SaxParser.parse(xmlFile, xsdFile);
    System.out.println(cards);
  }
}
