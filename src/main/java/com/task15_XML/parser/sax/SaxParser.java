package com.task15_XML.parser.sax;

import com.task15_XML.model.Card;
import com.task15_XML.xmlValidator.XmlValidator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SaxParser {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Card> parse(File xml, File xsd){
    List<Card> cards = new ArrayList<>();
    try {
    saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
    saxParserFactory.setValidating(true);

    SAXParser saxParser = saxParserFactory.newSAXParser();
    SaxHandler saxHandler = new SaxHandler();
    saxParser.parse(xml, saxHandler);
    cards = saxHandler.getCards();
} catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    }
    return cards;
  }
}
