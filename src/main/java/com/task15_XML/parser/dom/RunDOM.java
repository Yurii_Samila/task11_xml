package com.task15_XML.parser.dom;

import com.task15_XML.model.Card;
import com.task15_XML.parser.dom.DOMParser;
import com.task15_XML.xmlValidator.XmlValidator;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class RunDOM {
  private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
  private static DocumentBuilder documentBuilder;



  public static void run() throws ParserConfigurationException {
    File xmlFile = new File("src\\main\\resources\\xml\\oldCards.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\oldCardsXSD.xsd");
    Document document = null;
    try {
      documentBuilder = factory.newDocumentBuilder();
      document = documentBuilder.parse(xmlFile);
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    if (XmlValidator.validate(document, xsdFile)){
      List<Card> cards = DOMParser.parse(document);
      System.out.println(cards);
    }else {
      System.out.println("Failed parsing");
    }

  }
}
