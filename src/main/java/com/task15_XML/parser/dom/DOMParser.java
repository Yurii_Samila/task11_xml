package com.task15_XML.parser.dom;


import com.task15_XML.model.Card;
import com.task15_XML.model.Type;
import com.task15_XML.model.Types;
import com.task15_XML.model.Valuables;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMParser {
  public static List<Card> parse(Document document) {
    List<Card> cards = new ArrayList<>();
    NodeList nodeList = document.getElementsByTagName("card");
  for (int i = 0; i < nodeList.getLength(); i++){
  Card card = new Card();
    Types type;
    Node node =nodeList.item(i);
    Element element = (Element) node;
    card.setTheme(element.getElementsByTagName("theme").item(0).getTextContent());
    type = getType(element.getElementsByTagName("types"));
    card.setType(type);
    card.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
    card.setYear(Integer.parseInt(element.getElementsByTagName("year").item(0).getTextContent()));
    card.setAuthor(element.getElementsByTagName("author").item(0).getTextContent());
    card.setValuables(getValuables(element.getElementsByTagName("valuables")));
    cards.add(card);
  }
  return cards;
  }

private static Types getType(NodeList nodeList){
  Types type = new Types();
  if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE){
  Element element = (Element) nodeList.item(0);
    String content = element.getElementsByTagName("type").item(0).getTextContent();
    Type cardType = Type.valueOf(content.toUpperCase());
    type.setType(cardType);
    type.setSent(Boolean.parseBoolean(element.getElementsByTagName("sent").item(0).getTextContent()));
  }
  return type;
}
private static List<Valuables> getValuables(NodeList nodeList){
  List<Valuables> valuables = new ArrayList<>();
  for (int i = 0; i < nodeList.getLength(); i++){

    Node item = nodeList.item(i);
    if (item.getNodeType() == Node.ELEMENT_NODE){
      Element element = (Element) item;
      Valuables valuable = new Valuables();
      valuable.setValuable(element.getTextContent());
      valuables.add(valuable);
    }
  }
  return valuables;
}
}
