package com.task15_XML.xmlTransformer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XmlTransformer {


  public static void transform() {
    Source xml = new StreamSource("src\\main\\resources\\xml\\oldCardsSorted.xml");
    Source xslt = new StreamSource("src\\main\\resources\\xml\\oldCardsXSL.xsl");

    convertXMLToHTML(xml, xslt);

  }
  private static void convertXMLToHTML(Source xml, Source xslt){

  StringWriter sw = new StringWriter();
  try {
    FileWriter fw = new FileWriter("src\\main\\resources\\xml\\oldCards.html");
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer(xslt);
    transformer.transform(xml, new StreamResult(sw));
    fw.write(sw.toString());
    fw.close();
    System.out.println("Transform success");
  }catch (IOException | TransformerConfigurationException e) {
    e.printStackTrace();
  } catch (TransformerFactoryConfigurationError e) {
    e.printStackTrace();
  } catch (TransformerException e) {
    e.printStackTrace();
  }

}
}
