package com.task15_XML.controller;
import com.task15_XML.parser.dom.RunDOM;
import com.task15_XML.parser.sax.RunSax;
import com.task15_XML.parser.stax.RunStax;
import javax.xml.parsers.ParserConfigurationException;

public class CompareParsers {

  public static void main(String[] args) throws ParserConfigurationException {
    RunSax.run();
    RunStax.run();
    RunDOM.run();
  }

}
