package com.task15_XML.controller;
import com.task15_XML.xmlTransformer.XmlTransformer;
import com.task15_XML.model.ToXMLConverter;

public class Converter {

  public static void main(String[] args) {
    ToXMLConverter.convert();
    XmlTransformer.transform();
  }

}
